#include <stdio.h>
#include <string.h>
int strip_digit(long cc_mod);
long get_cc();
char validate(long cc);

int main()
{
    long cc;
    char valid;
    cc = get_cc();
    valid = validate(cc);
    if (valid == 'v')
    {
        printf("VISA\n");
    }
    else if (valid == 'd')
    {
        printf("DISCOVER\n");
    }
    else if (valid == 'm')
    {
        printf("MASTERCARD\n");
    }
    else if (valid == 'a')
    {
        printf("AMEX\n");
    }
    else if (valid == 'i')
    {
        printf("INVALID\n");
    }
}

int strip_digit(long cc_mod)
{
    long strip;
    strip = (cc_mod%10);
    return strip;
}

long get_cc()
{
    long cc;
    printf("Enter a credit card number: ");
    scanf("%ld", &cc);
    return cc;
}

char validate(long cc)
{
    long credit_card = cc;
    char type;
    int n;
    int count = 0;
    // printf("%ld\n", cc);
    while(cc != 0)
    {
        cc /= 10;
        count++;
        
    }
    // printf("%d\n", count);
    if (count < 15)
    {
        type = 'i';
        return type;
    }
    else
    {
        // Starting from the right...
        long checkSum;
        long credit_card_modified = credit_card;
        long final_digit, previous_digit;
        checkSum = (credit_card%10);
        long even;
        long even_calculated;
        long first_digit;
        long second_digit;
        long odd;
        long sum;
        // printf("Checksum: %ld\n", checkSum);

        for (int i = 1; i <= count; i++)
        {

            previous_digit = credit_card_modified%10;
            
            if (i%2 == 0)
            {
                // printf("Previous Digit before Multiplication: %ld\n", previous_digit);
                first_digit = previous_digit*2;
                second_digit = first_digit;
                first_digit = first_digit%10;
                // printf("Even Modified Digit 1: %ld\n", first_digit);
                first_digit = first_digit%10;
                second_digit = (second_digit/10);
                // printf("Even Modified Digit 2: %ld\n", second_digit);
                // printf("%d: %ld -> %ld\n", i, previous_digit, even_calculated);
                even_calculated = even_calculated + first_digit + second_digit;
                // printf("Even Calculated: %ld\n", even_calculated);
            }
            else
            {
                odd = odd + previous_digit;
                // printf("%d: %ld -> %ld\n", i, previous_digit, odd);
                
            }
            if (i == count)
            {
                final_digit = credit_card_modified;
            }
            credit_card_modified = (credit_card_modified/10);
            
        }
        sum = even_calculated + odd;
        // printf("Sum: %ld\n", sum);
        if (sum%10 == 0)
        {
            if (final_digit == 3)
            {
                type = 'a';
            }
            else if (final_digit == 4)
            {
                type = 'v';
            }
            else if (final_digit == 5)
            {
                type = 'm';
            }
            else if (final_digit == 6)
            {
                type = 'd';
            }
        }
        else
        {
             type = 'i';
        }
        return type;
    }
    // printf("%d\n", count);
    
}