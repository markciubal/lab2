#include <stdio.h>

void next_collatz(int next);
int get_start();
int counter = 1;

int main()
{
    get_start();
}

void next_collatz(int next)
{   
    if (next%2 == 0) // If the number is even.
    {
        printf("%d, ", next);
        next = next/2;
        counter++;
        next_collatz(next);
    }
    else // The number is odd.
    {
        if (next == 1)
        {
            printf("1\n");
        }
        else
        {
            printf("%d, ", next);
            next = (next*3)+1;
            counter++;
            next_collatz(next);
        }
    }
}

int get_start()
{
    int start_number;
    printf("Enter the starting number: ");
    scanf("%d", &start_number);
    if (start_number > 0)
    {
        printf("Collatz sequence: ");
        next_collatz(start_number);
        printf("Length: %d\n", counter);
        return 0;
    }
    else
    {
        printf("The number should be a positive integer.\n");
        get_start();
        return 0;
    }
}